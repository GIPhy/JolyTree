# JolyTree

_JolyTree_ (named in memory of [Nicolas Joly](https://research.pasteur.fr/en/member/nicolas-joly/)) is a command line script written in [Bash](https://www.gnu.org/software/bash/) that allows a distance-based phylogenetic tree with branch supports to be quickly inferred from non-aligned genome sequences.
_JolyTree_ runs on UNIX, Linux and most OS X operating systems.
For more details, see the associated publication (Criscuolo [2019](https://riojournal.com/article/36178/), [2020](https://f1000research.com/articles/9-1309)).

## Installation and execution

**A.** Install the following programs and tools, or verify that they are already installed with the required version:
* [mash](http://mash.readthedocs.io/en/latest/) [(Ondov et al. 2016)](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0997-x) version >= 1.0.2;
  * binaries: [github.com/marbl/Mash/releases](https://github.com/marbl/Mash/releases)
  * sources: [github.com/marbl/Mash](https://github.com/marbl/Mash)
* [gawk](https://www.gnu.org/software/gawk/manual/) version >= 4.1.0
  * sources: [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)
* [FastME](http://www.atgc-montpellier.fr/fastme/usersguide.php) [(Lefort et al. 2015)](https://doi.org/10.1093/molbev/msv150) version >= 2.1.5.1
  * sources: [gite.lirmm.fr/atgc/FastME](https://gite.lirmm.fr/atgc/FastME)
* [REQ](https://research.pasteur.fr/en/tool/r%ce%b5q-assessing-branch-supports-o%c6%92-a-distance-based-phylogenetic-tree-with-the-rate-o%c6%92-elementary-quartets/) version >= 1.2
  * sources: [gitlab.pasteur.fr/GIPhy/REQ](https://gitlab.pasteur.fr/GIPhy/REQ)

**B.** Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/JolyTree.git
```

**C.** If at least one of the four required binaries (step A) is not available on your `$PATH` variable, edit the file `JolyTree.sh` and indicate the local path to the `mash`, `gawk`, `FastME` and/or `REQ` binary(ies) (approximately between lines 130 and 230):

```bash
#############################################################################################################
#                                                                                                           #
# ================                                                                                          #
# = INSTALLATION =                                                                                          #
# ================                                                                                          #
#                                                                                                           #
# [1] REQUIREMENTS =======================================================================================  #
# JolyTree depends on Mash, gawk,  FastME and REQ (see below),  each with a minimum version required.  You  #
# should have them installed on your computer prior to using JolyTree. Make sure that each is installed on  #
# your $PATH variable, or specify below the full path to each of them.                                      #
#                                                                                                           #
# -- Mash: fast pairwise p-distance estimation --------------------------------------------------------     #
#    VERSION >= 1.0.2                                                                                       #
#    src: github.com/marbl/Mash                                                                             #
#                                                                 ################################################
                                                                  ################################################
  MASH=mash;                                                      ## <=== WRITE HERE THE PATH TO THE MASH       ##
                                                                  ##      BINARY (VERSION 1.0.2 MINIMUM)        ##
                                                                  ################################################
                                                                  ################################################
#                                                                                                           #
# -- gawk: fast text file processing ------------------------------------------------------------------     #
#    VERSION >= 4.1.0                                                                                       #
#    src: ftp.gnu.org/gnu/gawk                                                                              #
#                                                                 ################################################
                                                                  ################################################
  GAWK=gawk;                                                      ## <=== WRITE HERE THE PATH TO THE GAWK       ##
                                                                  ##      BINARY (VERSION 4.1.0 MINIMUM)        ##
                                                                  ################################################
                                                                  ################################################
#                                                                                                           #
# -- FastME: fast distance-based phylogenetic tree inference ------------------------------------------     #
#    VERSION >= 2.1.5.1                                                                                     #
#    src: gite.lirmm.fr/atgc/FastME/                                                                        #
#                                                                 ################################################
                                                                  ################################################
  FASTME=fastme;                                                  ## <=== WRITE HERE THE PATH TO THE FASTME     ##
                                                                  ##      BINARY (VERSION 2.1.5.1 MINIMUM)      ##
                                                                  ################################################
                                                                  ################################################
#                                                                                                           #
# -- REQ: fast computation of the rates of elementary quartets ----------------------------------------     #
#    VERSION >= 1.2                                                                                         #
#    src: gitlab.pasteur.fr/GIPhy/REQ                                                                       #
#                                                                 ################################################
                                                                  ################################################
  REQ=REQ;                                                        ## <=== WRITE HERE THE PATH TO THE REQ        ##
                                                                  ##      BINARY (VERSION 1.2 MINIMUM)          ##
                                                                  ################################################
                                                                  ################################################
#                                                                                                           #
#############################################################################################################

```

**D.** Give the execute permission to the file `JolyTree.sh`:
```bash
chmod +x JolyTree.sh
```

**E.** Execute _JolyTree_ with the following command line model:
```bash
./JolyTree.sh  [options]
```

## Usage

Launch _JolyTree_ without option to read the following documentation:

```
 USAGE:  JolyTree.sh  -i <directory>  -b <basename>  [options]

 OPTIONS:

    -i <directory>  directory name containing  FASTA-formatted contig files;  only files
                    ending with .fa, .fna, .fas or .fasta will be considered (mandatory)
    -b <basename>   basename of every written output file (mandatory)
    -q <real>       probability of observing a random k-mer (default: 0.000000001)
    -k <int>        k-mer size (default: estimated from the largest genome size with the
                    probability set by option -q)
    -s <real|int>   sketch size estimated as the  proportion (up to 1.00) of the average 
                    genome size;  the sketch size (integer > 2) can also be directly set 
                    using this option (default: 0.25)
    -c <real>       if at least one of the estimated p-distances is above this specified
                    cutoff, then a F81/EI correction is performed (default: 0.1)
    -a <real>       F81/EI transformation gamma shape parameter (default: 1.5)
    -f              using  the  four nucleotide  frequencies in  F81/EI  transformations  
                    (default:  to  deal  with  multiple  contig  files,   only  the  two 
                    frequencies A+T and C+G are used)
    -n              no BME tree inference (only pairwise distance estimates)
    -r <int>        number of steps  when performing the  ratchet-based  BME tree search
                    (default: 100)
    -t <int>        number of threads (default: 2)
```

## Notes

* In short, _JolyTree_ uses [_mash_](http://mash.readthedocs.io/en/latest/) to decompose each genome into a sketch of _k_-mers (options `-k`, `-q`, `-s`) and quickly estimate the _p_-distance between each pair of genomes. If required, every _p_-distance is transformed into an evolutionary distance (options `-c`, `-a`, `-f`). A ratchet-based optimal phylogenetic tree search is performed from the pairwise evolutionary distances using [_FastME_](http://www.atgc-montpellier.fr/fastme/usersguide.php) (option `-r`). Branch supports are finally estimated using [_REQ_](https://research.pasteur.fr/en/tool/r%ce%b5q-assessing-branch-supports-o%c6%92-a-distance-based-phylogenetic-tree-with-the-rate-o%c6%92-elementary-quartets/).

* It is not recommended to modify the option `-k`. The optimal value of _k_ is automatically estimated by equation (2) in [Ondov et al. (2016)](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0997-x) from the desired probability _q_ of observing a random _k_-mer (option `-q`). Since _JolyTree_ v2.0, the default probability is _q_ = 0.000000001, as it enables pairwise distances to be conveniently estimated in many cases. Increasing _q_ is not recommended, but can be useful to minimize the number of unknown distances (i.e. non-overlapping _k_-mer sets) when dealing with distantly-related genome sequences. Lowering _q_ leads to larger _k_-mer size that can be useful when dealing with very closely-related genome sequences.

* Default sketch size is 25% of the average genome size, which enables pairwise distances to be efficiently estimated with fast running times. Increasing the sketch size (option `-s`) yields more accurate estimates (especially with distantly-related genomes), but requires important disk space to store the sketch files. 

* Lowering the cutoff value for correcting the evolutionary distances (option `-c`) does generally not modify the inferred phylogenetic tree; on the other side, it is inadvisable to increase this cutoff value.

* The option `-c` allows multiple substitutions per character to be accurately estimated when an observed _p_-distance is quite large (e.g. > 0.1; see [Figure 3.1](https://books.google.fr/books?id=3Xc8DwAAQBAJ&pg=PA41) in Nei and Kumar 2000). In such cases, all the _p_-distances _p_ estimated by _mash_ are transformed into proper evolutionary distances _d_. Since _JolyTree_ v2.0, four _p_-distance transformations can be obtained using options `-c` and/or `-a`:
<div align="center">
<sub>

| correction       | formula                                                                 | options                                               | notes                                                                                             |
|:-----------------|:------------------------------------------------------------------------|:------------------------------------------------------|:--------------------------------------------------------------------------------------------------|
| none             | _d_ = _p_                                                               | `-c 1 -a $a` with any `$a` > 0                        | unaccurate estimate when _p_ > 0.1                                                                |
| Poisson          | _d_ = - log<sub>_e_</sub>(1 - _p_)                                      | `-a 0` (use `-c 1` to force Poisson correction)       | default transformation of _mash_                                                                  |
| F81/EI           | _d_ = -_b_<sub>1</sub> log<sub>_e_</sub>(1 - _p_/_b_<sub>2</sub>)       | `-a 0` (use `-c 0` to force F81/EI correction)        | formula (4) in [Tamura and Kumar (2002)](https://academic.oup.com/mbe/article/19/10/1727/1258975) |
| F81/EI + &Gamma; | _d_ = -_ab_<sub>1</sub>[(1 - _p_/_b_<sub>2</sub>)<sup>-1/_a_</sup> - 1] | `-a $a` to set &Gamma; shape parameter (default: 1.5) | formula (3) in [Criscuolo 2020](https://f1000research.com/articles/9-1309)                                                      |

</sub>
</div>

* The F81 corrections estimate pairwise distances based on the Equal-Input (EI) model of nucleotide substitution ([Felsenstein 1981](https://link.springer.com/article/10.1007/BF01734359); [Tajima and Nei 1982](https://link.springer.com/article/10.1007/BF01810830), [1984](https://academic.oup.com/mbe/article/1/3/269/1244029), [Tamura and Kumar 2002](https://academic.oup.com/mbe/article/19/10/1727/1258975)). These transformations were chosen because they can be directly computed from _p_-distances, and take into account unequal base frequencies and heterogeneous base composition among lineages (option `-f`; for details about the values _b_<sub>1</sub> and _b_<sub>2</sub>, see e.g. [Tamura and Kumar 2002](https://academic.oup.com/mbe/article/19/10/1727/1258975), [Criscuolo 2019](https://riojournal.com/article/36178/)). Thanks to the use of the supplementary gamma shape parameter (option `-a`), F81/EI gamma distance allows approximating evolutionary distances derived from complex nucleotide substitution models (the default parameter _a_ = 1.5 enables pairwise distances to be conveniently estimated in many cases; [Criscuolo 2020](https://f1000research.com/articles/9-1309)).

* Fast running times will be observed when using multiple threads. Since _JolyTree_ v2.0, almost all steps benefit from a large number of threads, i.e. genome parsing and sketching, _p_-distance estimates, and ratchet-based BME tree search.

* The verbosity of _JolyTree_ can be reduced by ending the command line by `2>/dev/null`

* To launch _JolyTree_ on multiple cores on computing facilities by [SLURM](https://slurm.schedmd.com), edit the file `JolyTree.sh` and read the subsection [3] of the _Installation_ section (approximately line 200).


## Example

In order to illustrate the usefulness of _JolyTree_ and to describe its output files, the following use case example describes its usage for inferring a phylogenetic tree of _Klebsiella_ genomes derived from the analysis of [Rodrigues et al. (2019)](https://doi.org/10.1016/j.resmic.2019.02.003).

##### Downloading genome sequences

The following [Bash](https://www.gnu.org/software/bash/) command lines allows the genome sequences of 40 _Klebsiella_ species (36 belonging to the _Klebsiella pneumoniae_ complex &ndash;Kp1 to Kp7&ndash; and 4 outgroup species &ndash;Kog&ndash;) to be downloaded from the [NCBI genome repository](https://www.ncbi.nlm.nih.gov/genome) inside a directory named _genomes_:

```bash
mkdir genomes/ ;
EUTILS="wget -q -O- https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&id=";
NCBIFTP="wget -q -O- https://ftp.ncbi.nlm.nih.gov/sra/wgs_aux/"; 
A1='^[A-Z]{2}[0-9]*$'; A2='^[A-Z]{6}[0-9]*$'; Z=".1.fsa_nt.gz";

t="Kp1-K.pneumoniae";
echo -e "SB4-2\tCAAHFS01\nATCC13883_T\tJOOW01\nMGH78578\tCP000647\nSB1139\tCAAHFT01\n5-2\tCAAHGI01\n04A025\tCAAHFZ01\n2-3\tCAAHGH01\nKp13\tCP003999\nNTUH-K2044\tAP006725\nBJ1-GA\tCAAHGC01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp2-K.quasipneumoniae.subsp.quasipneumoniae";
echo -e "01A030_T\tCCDF01\nSB1124\tCAAHFU01\nU41\tCAAHGA01\n18A069\tCAAHGF01\n0320584\tCAAHGK01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp3-K.variicola";
echo -e "01A065\tCAAHFX01\nF2R9_T\tCAAHGE01\n342\tCP000964\nAt-22\tCP001891" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp4-K.quasipneumoniae.subsp.similipneumoniae";
echo -e "09A323\tCAAHFV01\n12A476\tCAAHFY01\n07A044_T\tCBZR01\nCIP110288\tCAAHGD01\n1-1\tCAAHGG01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp5-K.variicola.subsp.tropicalensis";
echo -e "CDC4241-71\tCAAHGJ01\n814\tCAAHGL01\n885\tCAAHGM01\n1266_T\tCAAHGN01\n1283\tCAAHGO01\n1375\tCAAHGP01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp6-K.quasivariicola"; 
echo -e "08A119\tCAAHGB01\n10982\tAKYX01\nKPN1705\tCP022823\n01-467-2ECBU\tCAAHGR01\n01-310MBV\tCAAHGS01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kp7-K.africanensis";
echo -e "200023\tCAAHGQ01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
 
t="Kog-K";
echo -e "oxytoca.ATCC13182\tCAAHFW01\naerogenes.ATCC13048\tQVMZ01\ngrimontii.06D021\tFZTC01\nmichiganensis.DSM25444\tPRDB01" |
  while read -r s a; do echo $t.$s;([[ $a =~ $A1 ]]&&$EUTILS$a||$NCBIFTP${a:0:2}/${a:2:2}/$([[ $a =~ $A2 ]]&&echo ${a:4:2}/)$a/$a$Z|zcat)>genomes/$t.$s.fa; done
```

##### Running _JolyTree_

The following command line allows the script `jolyTree.sh` to be launched with default options on 8 threads:
```bash
./JolyTree.sh  -i genomes  -b klebsiella  -t 8  2>/dev/null
```
Of note, the verbosity could be expanded by omitting the final `2>/dev/null`.

As the basename was set to 'klebsiella', _JolyTree_ writes in few minutes the four following output files:

* `klebsiella.acgt`: the A, C, G and T residue counts for each genome,
* `klebsiella.oepl`: every pairwise _p_-distance in [OEPL (One Entry Per Line) format](http://giphy.pasteur.fr/faq/phylogenetics/distance-matrix-file-conversion/#how-to-deal-with-the-one-entry-per-line-oepl-matrix-format)
* `klebsiella.d`: the matrix of (corrected) pairwise evolutionary distances in PHYLIP square format
* `klebsiella.nwk`: the BME phylogenetic tree in NEWICK format with REQ confidence support at branches



## References

Criscuolo A (2019) A fast alignment-free bioinformatics procedure to infer accurate distance-based phylogenetic trees from genome assemblies. Research Ideas and Outcomes, 5:e36178. [doi:10.3897/rio.5.e36178](https://riojournal.com/article/36178/).

Criscuolo A (2020) On the transformation of MinHash-based uncorrected distances into proper evolutionary distances for phylogenetic inference. F1000Research, 9:1309. [doi:10.12688/f1000research.26930.1](https://doi.org/10.12688/f1000research.26930.1).

Felsenstein J (1981) Evolutionary trees from DNA sequences: a maximum likelihood approach. Journal of Molecular Evolution, 17(6):368-376. [doi:10.1007/BF01734359](https://link.springer.com/article/10.1007/BF01734359).

Lefort V, Desper R, Gascuel O (2015) FastME 2.0: a comprehensive, accurate, and fast distance-based phylogeny inference program. Molecular Biology and Evolution, 32(10):2798-2800. [doi:10.1093/molbev/msv150](https://doi.org/10.1093/molbev/msv150).

Nei M, Kumar S (2000) Molecular Evolution and Phylogenetics. Oxford University Press, Oxford. ISBN: 0-19-513584-9.

Ondov BD, Treangen TJ, Melsted P, Mallonee AB, Bergman NH, Koren S, Phillippy AM (2016) Mash: fast genome and metagenome distance estimation using MinHash. Genome Biology, 17(1):132. [doi:10.1186/s13059-016-0997-x](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0997-x).

Rodrigues C, Passet V, Rakotondrasoa A, Abdoulaye Diallo T, Criscuolo A, Brisse S (2019) Description of _Klebsiella africanensis_ sp. nov., _Klebsiella variicola_ subsp. _tropicalensis_ subsp. nov. and _Klebsiella variicola_ subsp. _variicola_ subsp. nov. Research in Microbiology. [doi:10.1016/j.resmic.2019.02.003](https://doi.org/10.1016/j.resmic.2019.02.003).

Tajima F, Nei M (1982) Biases of the estimates of DNA divergence obtained by the restriction enzyme technique. Journal of Molecular Evolution, 18(2):115-120. [doi:10.1007/BF01810830](https://link.springer.com/article/10.1007/BF01810830).

Tajima F, Nei M (1984) Estimation of evolutionary distance between nucleotide sequences. Molecular Biology and Evolution, 1(3):269-285. [doi:10.1093/oxfordjournals.molbev.a040317](https://academic.oup.com/mbe/article/1/3/269/1244029).

Tamura K, Kumar S (2002) Evolutionary distance estimation under heterogeneous substitution pattern among lineages. Molecular Biology and Evolution, 19(10):1727-1736. [doi:10.1093/oxfordjournals.molbev.a003995](https://academic.oup.com/mbe/article/19/10/1727/1258975).



## Gallery

Below is a list of some phylogenetic trees inferred using _JolyTree_:

* [_Corynebacteria_ genus](https://www.microbiologyresearch.org/docserver/fulltext/ijsem/68/12/ijsem003069-f1.gif) ([Dazas et al. 2018](https://doi.org/10.1099/ijsem.0.003069))

* [_Mucor circinelloides_ f. _circinelloides_](https://mbio.asm.org/content/mbio/9/2/e00573-18/F3.large.jpg) ([Garcia-Hermoso et al. 2018](https://doi.org/10.1128/mBio.00573-18))

* [_Aggregatibacter_](https://riojournal.com/article/36178/zoom/fig/4969692/), [_Borreliella_](https://riojournal.com/article/36178/zoom/fig/4969672/), [_Elizabethkingia_](https://riojournal.com/article/36178/zoom/fig/4969676/), [_Lactococcus_](https://riojournal.com/article/36178/zoom/fig/4969680/), [_Providencia_](https://riojournal.com/article/36178/zoom/fig/4969684/), and [_Ralstonia_](https://riojournal.com/article/36178/zoom/fig/4969688/) genera ([Criscuolo 2019](https://riojournal.com/article/36178/))

* [_Escherichia coli_](https://wwwnc.cdc.gov/eid/article/25/1/18-0534-f2) ([Nadimpali et al. 2019](https://doi.org/10.3201/eid2501.180534))

* [_Klebsiella pneumoniae_](https://aem.asm.org/content/aem/86/7/e02711-19/F1.large.jpg) ([Barbier et al. 2020](https://doi.org/10.1128/AEM.02711-19))

* [_Rathayibacter_ genus](https://mra.asm.org/content/ga/9/22/e00316-20/F1.large.jpg) ([Tarlachkov et al. 2020](https://doi.org/10.1128/MRA.00316-20))

* [_Corynebacterium diphteriae_ complex](https://ars.els-cdn.com/content/image/1-s2.0-S092325082030019X-gr1_lrg.jpg) ([Badell et al. 2020](https://doi.org/10.1016/j.resmic.2020.02.003))

* [_Klebsiella pneumoniae_](https://www.tandfonline.com/na101/home/literatum/publisher/tandf/journals/content/kgmi20/2020/kgmi20.v011.i05/19490976.2020.1748257/20200625/images/medium/kgmi_a_1748257_f0002_c.jpg) ([Huynh et al. 2020](https://doi.org/10.1080/19490976.2020.1748257 ))

* [_Stromatolite_ bacterial communities](https://www.biorxiv.org/content/biorxiv/early/2020/03/14/818625/F5.large.jpg) ([Waterworth et al. 2020](https://doi.org/10.1101/818625))

* [_Campylobacter_ genus](https://figshare.com/articles/Data_Sheet_1_Taking_Control_Campylobacter_jejuni_Binding_to_Fibronectin_Sets_the_Stage_for_Cellular_Adherence_and_Invasion_pdf/12103260) ([Konkel et al. 2020](https://doi.org/10.3389/fmicb.2020.00564.s001))

* [_Flavobacterium_ genus](https://research.pasteur.fr/wp-content/uploads/2020/10/research_pasteur-flavobacterium-panici-sp-nov-isolated-from-the-rhizosphere-of-the-switchgrass-panicum-virgatum-flavo.pxu-55-scaled.jpg) (Kämpfer et al. [2020a](https://www.microbiologyresearch.org/content/journal/ijsem/10.1099/ijsem.0.004510), [2020b](https://www.microbiologyresearch.org/content/journal/ijsem/10.1099/ijsem.0.004482))

* [_Phialophora_ genus](https://onlinelibrary.wiley.com/cms/asset/d3179207-551c-4887-90b5-a2877ae0f807/myc13244-fig-0002-m.jpg) ([Song et al. 2021](https://onlinelibrary.wiley.com/doi/10.1111/myc.13244))

